package net.cubekrowd.cksleigh;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;

public class EventListener implements Listener {

    public CKSleigh plugin;

    public EventListener(CKSleigh plugin) {
        this.plugin = plugin;
    }

    public static boolean isSign(Material sign) {
        return Tag.SIGNS.isTagged(sign) || Tag.WALL_SIGNS.isTagged(sign);
    }

    @EventHandler
    public void onSignPlace(SignChangeEvent e) {
        var p = e.getPlayer();
        var sb = e.getBlock();

        if (!(p.hasPermission("cksleigh.setspawner"))) {
            return;
        }

        if (!(e.getLine(0).equalsIgnoreCase("cksleigh"))) {
            return;
        }

        if (e.getLine(1).equalsIgnoreCase("spawner")) {
            var name = e.getLine(2);
            var key = "spawners." + name;
            var spawnerSection = plugin.getConfig().getConfigurationSection(key);
            if (spawnerSection != null) {
                p.sendMessage(CKSleigh.SLEIGH_PREFIX + ChatColor.RED + "Spawner " + ChatColor.GOLD + name + ChatColor.RED + " already exists");
                return;
            }

            spawnerSection = plugin.getConfig().createSection(key);
            spawnerSection.set("loc", sb.getLocation());
            plugin.saveConfig();
            plugin.addSpawner(name, spawnerSection);
            plugin.spawnSleigh(sb.getLocation());
            p.sendMessage(CKSleigh.SLEIGH_PREFIX + ChatColor.GREEN + "Spawner location set to " + ChatColor.GOLD + sb.getX() + ", " + sb.getY() + ", " + sb.getZ());
        }
    }

    @EventHandler
    public void onSignBreak(BlockBreakEvent e) {
        var p = e.getPlayer();
        var b = e.getBlock();

        if (!isSign(b.getType())) {
            return;
        }

        var loc = b.getLocation();
        var spawners = plugin.spawners;
        for (int i = 0; i < spawners.size(); i++) {
            var spawner = spawners.get(i);
            if (spawner.loc.equals(loc)) {
                if (!(p.hasPermission("cksleigh.removesign"))) {
                    e.setCancelled(true);
                } else {
                    plugin.removeSpawner(i, spawner);
                    p.sendMessage(CKSleigh.SLEIGH_PREFIX + ChatColor.RED + "Spawner " + ChatColor.GOLD + spawner.name + ChatColor.RED + " has been removed");
                }
                return;
            }
        }
    }

    @EventHandler
    public void onVehicleExit(VehicleExitEvent e) {
        var v = e.getVehicle();

        if (!(v instanceof Boat)) { return; }
        for (CKSleighSpawner memSpawner : plugin.spawners) {
            if (v.equals(memSpawner.boat)) {
                // Delays AND nested ifs!
                Bukkit.getScheduler().runTaskLater(this.plugin, () -> {
                    if (v.getPassengers().isEmpty()) {
                        v.teleport(memSpawner.loc.clone().add(0.5,2,0.5));
                    }
                }, 1L);
            }
        }
    }

    @EventHandler
    public void onLogout(PlayerQuitEvent e) {
        var p = e.getPlayer();
        var v = p.getVehicle();
        if (!v.getPassengers().isEmpty() && v.getPassengers().contains(p)) { v.removePassenger(p); }
    }
}
