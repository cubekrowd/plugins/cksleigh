package net.cubekrowd.cksleigh;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public final class CKSleigh extends JavaPlugin {

    public static String SLEIGH_PREFIX = ChatColor.DARK_GRAY + "[" + ChatColor.DARK_AQUA + "CK" + ChatColor.GOLD + "Sleigh" + ChatColor.DARK_GRAY + "] ";
    public List<CKSleighSpawner> spawners = new ArrayList<>();

    @Override
    public void onEnable() {
        getCommand("respawnsleighs").setExecutor(this);
        getServer().getPluginManager().registerEvents(new EventListener(this), this);
        getConfig().options().copyDefaults();
        saveDefaultConfig();

        var configSpawners = getConfig().getConfigurationSection("spawners");
        if (configSpawners != null) {
            for (var name : configSpawners.getKeys(false)) {
                var section = configSpawners.getConfigurationSection(name);
                addSpawner(name, section);
            }
        }
        respawnAllSleighs();
    }

    @Override
    public void onDisable() {
        killAllSleighs();
        spawners.clear();
    }

    public void addSpawner(String name, ConfigurationSection section) {
        var spawner = new CKSleighSpawner();
        spawner.loc = section.getLocation("loc");
        spawner.name = name;
        spawners.add(spawner);
    }

    public void removeSpawner(int i, CKSleighSpawner spawner) {
        if (spawners.contains(spawner)) { spawners.remove(i); }

        var key = "spawners." + spawner.name;
        getConfig().set(key, null);
        saveConfig();
    }

    public void killAllSleighs() {
        for (var memSpawner : spawners) {
            memSpawner.boat.remove();
            memSpawner.boat = null;
        }
    }

    public void spawnSleigh(Location spawner) {

        if (spawner.getWorld() == null) { return; }

        for (CKSleighSpawner memSpawner : spawners) {
            if (memSpawner.loc.equals(spawner)) { memSpawner.boat = spawner.getWorld().spawn(spawner.clone().add(0.5,2,0.5), Boat.class); }
        }
    }

    public void respawnAllSleighs() {
        for (var memSpawner : spawners) {
            if (memSpawner.loc.getWorld() == null) { return; }
            if (memSpawner.boat == null) { spawnSleigh(memSpawner.loc); }
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        var p = (Player) sender;

        if (!(p.hasPermission("cksleigh.respawnsleighs"))) {
            p.sendMessage(ChatColor.RED + "You do not have permission to use this command");
            return true;
        }

        killAllSleighs();
        respawnAllSleighs();
        p.sendMessage(SLEIGH_PREFIX + ChatColor.GREEN + "Respawned all sleighs");

        return true;
    }
}
